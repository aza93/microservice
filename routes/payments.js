var express = require('express');
const { createPayment, listPayments } = require('../payment');
var router = express.Router();

/* GET payments listing. */
router.get('/', function (req, res, next) {
    const payments = listPayments();
    res.send(payments);
});

router.get('/:id', (req, res) => {
    const { id } = req.params

    try {
        const payment = getPayment(id)

        res.send(payment)
    } catch (error) {
        res.send({ error: error.message });
    }
})

router.post('/', (req, res) => {
    const data = req.body

    try {
        const newPayment = createPayment(data)

        res.send(newPayment);
    } catch (error) {
        res.send({ error: error.message });
    }
})

module.exports = router;
