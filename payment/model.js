const is = require('is_js')
const { v4: uuidv4 } = require('uuid');
const { listProducts } = require('../product');
const { listUser } = require('../user/model');
const payments = []

const paymentCreator = (data) => {
    const { buyerId, productId, payed } = data

    if (!buyerId || !productId) {
        throw new Error('No id specified')
    }

    if (is.empty(listUser(buyerId)) || is.empty(listProducts(productId))) {
        throw new Error('Buyer or Product not found')
    }

    const payment = {
        id: uuidv4(),
        date: Date.now(),
        buyerId,
        productId,
        payed
    }

    Object.freeze(payment)

    payments.push(payment)

    return payment
}

module.exports = {
    paymentCreator,
    payments
}