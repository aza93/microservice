const { bought } = require("../product")
const { canBuy } = require("../product/model")
const { paymentCreator } = require("./model")


const createPayment = (data) => {
    const { payed, productId } = data

    if (!canBuy(payed, productId)) {
        throw new Error('Cannot buy')
    }

    const payment = paymentCreator(data)
    bought(productId)
    return payment
}

module.exports = {
    createPayment
}